const arr=["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnipro"];

function addList(arr){
let list=document.createElement('ul')
document.body.append(list)
let newList = arr.map(function (item) {
    if (Array.isArray(item)) {
            let subList = item.map(function (subitem) {
                return `<li>${subitem}</li>`;
            });
            return `<li><ul>${subList.join('')}</ul></li>`;
        }
    return `<li>${item}</li>`
})
list.innerHTML = newList.join('');
}

let showTime = (sec) => {
    let container = document.createElement('div');
    document.body.append(container);
    container.innerHTML = 'Self-destruction in: ';
    let timer = document.createElement('span');
    container.append(timer);
    timer.innerHTML = sec; 
    let timeLeft = +(timer.innerHTML);

    let timerFunc = setInterval(() => {
        if (timeLeft >= 1) {
            timer.innerHTML = timeLeft--;
        } else {
            document.body.innerHTML = '';
            clearInterval(timerFunc);
        }
    }, 1000)
}

showTime (3)

addList(arr)